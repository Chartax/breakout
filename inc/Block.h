#include "Color.h"
#include "Rectangle.h"
#ifndef BLOCK_H
#define BLOCK_H
class Block
{
	private:
		int X;
		int Y;
		Color* BlockColor;
		Block() { }
	public:
		// ctor
		Block(int x, int y);

		Rectangle* GetRect() { return new Rectangle(X * 50, Y * 50, 50, 50); }

		int GetX() { return X; }
		int GetY() { return Y; }
		void SetX(int x);
		void SetY(int y);
		void SetColor(int r, int g, int b);
		Color* GetColor() { return BlockColor; }
};
#endif
