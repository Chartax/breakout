#ifndef COLOUR_H
#define COLOUR_H
// Later, add HSL, CMYK and Hex? 
class Color
{
	private:
		int R;
		int G;
		int B;
		Color() { }
	public:
		int GetR() { return R; }
		int GetG() { return G; }
		int GetB() { return B; }
		void SetR(int r);
		void SetG(int g);
		void SetB(int b);
		void SetRGB(int r, int g, int b);
		Color(int r, int g, int b);
};
#endif
