#include <SDL2/SDL.h>
#ifndef RECTANGLE_H
#define RECTANGLE_H
class Rectangle
{
	private:
		int X;
		int Y;
		int Width;
		int Height;
		Rectangle() { }
	public:
		int GetX() { return X; }
		int GetY() { return Y; }
		int GetWidth() { return Width; }
		int GetHeight() { return Height; }
		void SetX(int x);
		void SetY(int y);
		void SetWidth(int width);
		void SetHeight(int height);
		void SetPosition(int x, int y);
		void SetSize(int width, int height);
		SDL_Rect ToSDL();
		Rectangle (int x, int y, int width, int height);
};
#endif
