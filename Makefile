CXX = c++
SRC = src/*.cc
INC = -I inc
LINK = -lSDL2
OUTDIR = bin
OUT = bin/breakout

all:
	$(CXX) $(SRC) $(INC) $(LINK) -o $(OUT)

clean:
	rm -rf $(OUTDIR)/*

run: all
	./$(OUT)
