#include <iostream>
#include <list>
#include <SDL2/SDL.h>
#include "Breakout.h"
#include "Block.h"

using namespace std;

int ROWS = 5;
int COLUMNS = 10;
int ColorStep = 255 / ROWS;
Block* Blocks [5][10];


SDL_Window *Window;
SDL_Renderer *Renderer;

int main(int argc, char** argv)
{
	if (InitSDL() == 0)
	{
		bool running = true;
		CreateBlocks();
		while (running)
		{
			Uint32 start = SDL_GetTicks();
			SDL_Event event;
			while(SDL_PollEvent(&event))
			{
				switch(event.type)
				{
					case SDL_QUIT:
						running = false;
						break;
					case SDL_KEYDOWN:
						switch (event.key.keysym.sym)
						{
							case SDLK_ESCAPE:
								running = false;
								break;
						}
				}
			}

			SDL_RenderClear(Renderer);

			for (int i = 0; i < ROWS; i++)
			{
				for (int j = 0; j < COLUMNS; j++)
				{
					Block* block = Blocks[i][j];
					Color* color = block->GetColor();

					SDL_SetRenderDrawColor(Renderer, color->GetR(), color->GetG(), color->GetB(), 255);
					SDL_Rect rect = block->GetRect()->ToSDL();
					cout << "Drawing rect at" << rect.x << ", " << rect.y << " with w" << endl;
					SDL_RenderDrawRect(Renderer, &rect);
				}
			}

			SDL_RenderPresent(Renderer);

			// Get the difference in the number of ticks between the start and now
			Uint32 delta = SDL_GetTicks() - start;
			// Divide 1000 by the number of FPS
			float msPerFrame = 1000 / 60;
			// If delta is less than the number of frames
			if(delta < msPerFrame)
			{
				// Delay by how many milliseconds delta was greater.
				SDL_Delay(msPerFrame - delta);
			}

		}
	}
	SDL_DestroyWindow(Window);
	SDL_Quit();
	return 0;
}

int InitSDL()
{
		if(SDL_Init(SDL_INIT_EVERYTHING) != 0)
		{
			cout << "SDL_Init Error: " << SDL_GetError() << endl;
			SDL_Quit();
			return 1;
		}
		else
		{
			SDL_Window *win = SDL_CreateWindow("Hello World!", 100, 100, 640, 480, SDL_WINDOW_SHOWN);
			if (win == NULL)
			{
				cout << "SDL_CreateWindow Error: " << SDL_GetError() << endl;
				SDL_Quit();
				return 1;
			}
			else
			{
				SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
				if (ren == NULL)
				{
					SDL_DestroyWindow(win);
					cout << "SDL_CreateRenderer Error: " << SDL_GetError() << endl;
					SDL_Quit();
					return 1;
				}
				else
				{
					Window = win;
					Renderer = ren;
					return 0;
				}
			}
		}
}

void ListBlocks()
{
	for (int i = 0; i <= ROWS; i++)
	{
		for (int j = 0; j <= COLUMNS; j++)
		{
			Block* currentBlock = Blocks[i][j];
			Color* blockColor = currentBlock->GetColor();
			int r = blockColor->GetR();
			int g = blockColor->GetG();
			int b = blockColor->GetB();
			Rectangle* blockRect = currentBlock->GetRect();
			int x = blockRect->GetX();
			int y = blockRect->GetY();
			cout << "Block at (" << i << ", " << j << ") is color "
				<< r << ", " << g << ", " << b <<
				" and is at " << x << " x " << y << "."
				<< endl;
		}
	}
}

void CreateBlocks()
{
	// Let's make 5 rows of 10 blocks
	for (int i = 0; i <= ROWS; i++)
	{
		int colorRed = ColorStep * i; 
		
		for (int j = 0; j <= COLUMNS; j++)
		{
			// Place a block here.
			Block* newBlock = new Block(i, j);
			newBlock->SetColor(colorRed, 255, 255);
			Blocks[i][j] = newBlock;
		}
	}
}
