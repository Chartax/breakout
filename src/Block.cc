#include "Block.h"

Block::Block(int x, int y)
{
	SetX(x);
	SetY(y);
}

void Block::SetX(int x)
{
	X = x;
}

void Block::SetY(int y)
{
	Y = y;
}

void Block::SetColor(int r, int g, int b)
{
	BlockColor = new Color(r, g, b);
}
