#include <Rectangle.h>

Rectangle::Rectangle(int x, int y, int width, int height)
{
	SetPosition(x, y);
	SetSize(width, height);
}

SDL_Rect Rectangle::ToSDL()
{
		SDL_Rect sdlRect;
		sdlRect.x = X;
		sdlRect.y = Y;
		sdlRect.w = Width;
		sdlRect.h = Height;

		return sdlRect;
}


void Rectangle::SetSize(int width, int height)
{
	SetWidth(width);
	SetHeight(height);
}

void Rectangle::SetPosition(int x, int y)
{
	SetX(x);
	SetY(y);
}

void Rectangle::SetX(int x)
{
	X = x;
}

void Rectangle::SetY(int y)
{
	Y = y;
}

void Rectangle::SetWidth(int width)
{
	Width = width;
}

void Rectangle::SetHeight(int height)
{
	Height = height;
}
