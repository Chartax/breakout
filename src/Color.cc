#include <Color.h>

Color::Color(int r, int g, int b)
{
	SetRGB(r, g, b);
}

void Color::SetRGB(int r, int g, int b)
{
	SetR(r);
	SetG(g);
	SetB(b);
}

void Color::SetR(int r)
{
	R = r;
}
void Color::SetG(int g)
{
	G = g;
}
void Color::SetB(int b)
{
	B = b;
}
